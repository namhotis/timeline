import date from './date.js';
import arrays from './arrays.js'

const tools = {
  date,
  arrays
};

export default tools;
