const arrays = {
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index
  },
  getUniquesValueFromArray(arr) {
    return arr.filter(function (elem, index, self) {
      return index === self.indexOf(elem);
    });
  },
  isEmpty(obj) {
    return obj &&
      Object.keys(obj).length === 0 &&
      obj.constructor === Object;
  },
  onlyExact(arr, term) { return arr.filter((instance) => instance.label === term)},
  sortByPertinence(arr) { return arr.sort((a, b) => Object.size(b) - Object.size(a))}
}

export default arrays
