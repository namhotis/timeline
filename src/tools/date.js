const date = {
  sortDateArray(array, typeOfSort = "startingDate") {
    return array
      .sort(
        (a, b) =>
          // Turn your strings into dates, and then subtract them
          // to get a value that is either negative, positive, or zero.
          new Date(b.date || b[typeOfSort]) - new Date(a.date || a[typeOfSort])
      )
      .reverse();
  },
  formatDate(date) {
    date = new Date(date);
      if (date !== null) {
        return {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDay(),
        };
      }
    },
    getEndingDate(time1, time2) { 
      console.log(time1, time2)
      if (time2 === null) {
        return true
      } else if (time1 === null) {
        return false
      } else {
        return new Date(time1) > new Date(time2);
      }
     }, // true if time1 is later,
     getStartingDate(time1, time2) { 
      console.log(time1, time2)
      if (time2 === null) {
        return true
      } else if (time1 === null) {
        return false
      } else {
        return new Date(time1) < new Date(time2);
      }
     } // true if time1 is later
};

export default date;
