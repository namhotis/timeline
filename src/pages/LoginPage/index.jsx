import React from 'react'
import Header from '../../components/Header'
import Login from '../../components/Login'

const LoginPage = () => {
  return (
    <main className="HomePage">
      <Header mainLogo={false} />
      <section className="homepage">
        <section
          className="homepageContainer"
          style={{ backgroundImage: 'url("/assets/img/background-homepage.jpg")' }}
        >
          <Login />        
        </section>
      </section>
    </main>
  )
}

export default LoginPage
