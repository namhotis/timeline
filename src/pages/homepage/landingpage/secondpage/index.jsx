import React from 'react'
import './index.scss'
import Footer from '../../../../components/Footer'
import LinkList from '../../../../components/LinkList'


const SecondPage = () => {

  var firstList = ['Rejoindre Timeline', 'Établissement scolaire'];
  var secondList = ['Informations', 'Nous contacter', 'Confidentialité', 'C.G.U'];


  return (
    <section className="homepageContainer secondPage" id="secondPage">
      <div className="listLinkContainer">
        <LinkList linkList={firstList} />
        <LinkList linkList={secondList} />
      </div>
      <Footer />
    </section>
  );
}

export default SecondPage;
