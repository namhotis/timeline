import React from 'react'
import './index.scss'
import FrontPage from './frontpage'
import SecondPage from './secondpage'

const HomePageContainer = () => (
  <section className="homepage">
    <FrontPage />
    <SecondPage />
  </section>
);

export default HomePageContainer;
