import React from 'react'
import SearchBarHome from '../../../../components/SearchBarHome';
import './index.scss'

const FrontPage = () => {
  function goToSecondPage() {
    var elmnt = document.getElementById("secondPage");
    elmnt.scrollIntoView({
      behavior: "smooth",
    });
  }

  return (
    <section
      className="homepageContainer"
      style={{ backgroundImage: 'url("/assets/img/background-homepage.jpg")' }}
    >
      <div className="searchContainer">
        <h1>Browse. Link. Learn.</h1>
        <SearchBarHome />
      </div>

      <a className="arrow-icon" onClick={goToSecondPage}>
        <span className="left-bar"></span>
        <span className="right-bar"></span>
      </a>
    </section>
  );
};

export default FrontPage;
