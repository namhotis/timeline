import React from 'react'
import './index.scss'

import Header from '../../components/Header'
import HomePageContainer from './landingpage'

const HomePage = () => (
  <main className="HomePage">
    <Header mainLogo={false} />
    <HomePageContainer />
  </main>
);

export default HomePage;
