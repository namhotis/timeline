import React from 'react'
import Header from '../../components/Header';
import Register from '../../components/Register'

const RegisterPage = () => {
  return (
    <main className="HomePage">
      <Header mainLogo={false} />
      <section className="homepage">
        <section
          className="homepageContainer"
          style={{ backgroundImage: 'url("/assets/img/background-homepage.jpg")' }}
        >
          <Register />
        </section>
      </section>
    </main>
  );
}

export default RegisterPage
