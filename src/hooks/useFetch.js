import { useState, useEffect } from 'react';

// This hooks allow us to fetch datas in any endpoint
//  Example of use :
//
// const { response, loading, error } = useFetch(
//  `${process.env.REACT_APP_API_URL}events`,
//  {}
// );
export default function useFetch(url, options = {}) {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const res = await fetch(url, options);
        const json = await res.json();
        setResponse(json);
        setIsLoading(false);
      } catch (err) {
        setError(err);
      }
    };
    fetchData();
  }, []);
  return { response, error, isLoading };
}
