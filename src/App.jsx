import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import './App.css';
import TimelineBuilder from './components/TimelineBuilder';
import HomePage from './pages/homepage';
import './index.scss'
import TimelinesProvider from './contexts/TimelinesContext'
import EventsProvider from "./contexts/EventsContext";
import { AuthProvider } from "./contexts/UserContext";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";


function App() {

  return (
    <AuthProvider>
      <TimelinesProvider>
        <EventsProvider>
          <Router>
            <Switch>
              <Route path="/timeline" component={TimelineBuilder} />
              <Route path="/login" component={LoginPage} />
              <Route path="/register" component={RegisterPage} />
              <Route path="/" component={HomePage} />
            </Switch>
          </Router>
        </EventsProvider>
      </TimelinesProvider>
    </AuthProvider>
  );
}

export default App;
