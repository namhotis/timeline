import React from "react";
import Login from "../components/Login";
import Timeline from "../Pages/timeline";
import PageNotFound from "../pages/NotFound";

const routes = [
  {
    path: "/login",
    component: Login,
    isPrivate: false,
  },
  {
    path: "/dashboard",
    component: Timeline,
    isPrivate: true,
  },
  {
    path: "/*",
    component: PageNotFound,
    isPrivate: true,
  },
];

export default routes;
