import React, { useReducer } from "react";
// import { reducer, fetchInitial } from "./EventsReducer";
import { AuthReducer, initialState } from './UserReducer'
import { loginUser, registerUser, logout } from "./AuthActions";

const AuthStateContext = React.createContext();
const AuthDispatchContext = React.createContext();

function useAuthState() {
  const context = React.useContext(AuthStateContext);
  if (context === undefined) {
    throw new Error("useAuthState must be used within a AuthProvider");
  }

  return context;
}

function useAuthDispatch() {
  const context = React.useContext(AuthDispatchContext);
  if (context === undefined) {
    throw new Error("useAuthDispatch must be used within a AuthProvider");
  }

  return context;
}

const AuthProvider = ({ children }) => {
  const [user, dispatch] = useReducer(AuthReducer, initialState);

  return (
    <AuthStateContext.Provider value={user}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
};

export {
  AuthProvider,
  useAuthState,
  useAuthDispatch,
  registerUser,
  loginUser,
  logout,
};
