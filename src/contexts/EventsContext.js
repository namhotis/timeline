import React, {createContext, useReducer} from 'react';
import { reducer } from "./EventsReducer";

const initialState = [];
export const EventsContext = createContext(initialState);

const Provider = ( { children } ) => {
  const [state, dispatch] = useReducer(reducer, [])

  // const getEvents = () => {
  //   fetch(`${process.env.REACT_APP_API_URL}events`)
  //     .then((response) => response.json())
  //     .then((response) => dispatch(fetchInitial(response)));
  // }
    
  // useEffect(() => {
    // getEvents();
  // }, []);

  return (
    <EventsContext.Provider value={{ state, dispatch }}>
      {children}
    </EventsContext.Provider>
  );
};

export default Provider;
