import React, {createContext, useReducer } from 'react';
import { reducer } from "./TimelinesReducer";

const initialState = {
  currentTimelines: [],
  currentTimeline: null
};
export const TimelinesContext = createContext(initialState);

const Provider = ( { children } ) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  return (
    <TimelinesContext.Provider value={{ state, dispatch }}>
      {children}
    </TimelinesContext.Provider>
  );
};

export default Provider;
