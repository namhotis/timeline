// action
const ADD_TIMELINE = "ADD_TIMELINE";
const UPDATE_TIMELINE = "UPDATE_TIMELINE"
const SELECT_TIMELINE = "SELECT_TIMELINE"

export const addTimeline = (data) => ({
  type: ADD_TIMELINE,
  data
})

export const updateTimeline = (data) => ({
  type: UPDATE_TIMELINE,
  data
})

export const selectTimeline = (data) => ({
  type: SELECT_TIMELINE,
  data,
});

export const reducer = (state, action) => {
  switch (action.type) {
    case SELECT_TIMELINE: {
      let idTimeline = null;

      let _timelines = {
        currentTimelines: [...state.currentTimelines],
        currentTimeline: state.currentTimeline,
      };
      
      for (var i in _timelines.currentTimelines) {
        if (_timelines.currentTimelines[i].title == action.data.title) {
          idTimeline = _timelines.currentTimelines[i].id;
        }
      }

      return {
        currentTimelines: state.currentTimelines,
        currentTimeline: idTimeline,
      };
    }
    case ADD_TIMELINE:
      return {
        currentTimelines: [...state.currentTimelines, action.data],
        currentTimeline: state.currentTimeline,
      };
    case UPDATE_TIMELINE: {
      let _timelines = {
        currentTimelines: [...state.currentTimelines],
        currentTimeline: state.currentTimeline,
      };

      for (var j in _timelines.currentTimelines) {
        if (_timelines.currentTimelines[j].title == action.data.title) {
          _timelines.currentTimelines[j] = {
            ..._timelines.currentTimelines[j],
            ...action.data
          }
          
          break; //Stop this loop, we found it!
        }
      }

      return _timelines;
    }
    default:
      return state;
  }
};
