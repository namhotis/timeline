import React, { useState, useEffect, useContext, useRef } from 'react'
import './index.scss'
import { useHistory } from 'react-router-dom';
import { addTimeline } from '../../contexts/TimelinesReducer'
import { TimelinesContext } from "../../contexts/TimelinesContext";

const SearchBarHome = () => {
  let history = useHistory();
  const timelinesGlobalState = useContext(TimelinesContext);
  const [showProposedTimelines, setShowProposedTimelines] = useState(false);
  const [searchBarValue, setSearchBarValue] = useState("");
  const [proposedTimelines, setProposedTimelines] = useState([]);

  const inputEl = useRef(null);

  useEffect(() => {
    inputEl.current.focus();
    console.log(timelinesGlobalState);
  }, []);

  useEffect(() => {
    getAutocomplete()
  }, [searchBarValue])

  const getAutocomplete = () => {
    fetch(
      `https://fr.wikipedia.org/w/api.php?action=query&origin=*&generator=search&gsrsearch=${searchBarValue}&prop=info&inprop=url&format=json`
    )
      .then((response) => response.json())
      .then((response) => {
        const propals = [];
          response?.query?.pages &&
          Object.values(response.query.pages).forEach((page) => {
            propals.push(page.title);
          });
        setProposedTimelines(propals);
      })
      .catch((err) => console.log(err));
  }

  const redirectToFrise = () => {
    timelinesGlobalState.dispatch(
      addTimeline({
        id: timelinesGlobalState.state.currentTimelines.length + 1,
        title: searchBarValue,
        isLoading: true,
      })
    );

    const location = {
      pathname: "/timeline",
      // state: { term: searchBarValue },
    };

    history.push(location);
  };

  const addFrise = (e, timeline) => {
    setShowProposedTimelines(false);

    setSearchBarValue(e, timeline);
  };

  useEffect(() => {

    if(showProposedTimelines == true) {
      searchBarValue === "" && setShowProposedTimelines(false);
    } else {
      proposedTimelines.length !== 0 && setShowProposedTimelines(true);
    }
  }, [searchBarValue]);

  useEffect(() => {
    if(proposedTimelines.length === 0) {
      setShowProposedTimelines(false);
    }
  }, [proposedTimelines]);

  return (
    <div className="search-bar">
      <div className="search-box">
        <input
          className="search-input"
          type="text"
          placeholder="Search something.."
          value={searchBarValue}
          onChange={(e) => setSearchBarValue(e.target.value)}
          ref={inputEl}
        />
        <button className="search-btn" onClick={redirectToFrise}>
            <img src={"/assets/img/icon-search.svg"} />
        </button>
      </div>
      {showProposedTimelines ? (
        <ul className="search-bar-results">
          {proposedTimelines.map((timeline, index) => {
            return (
              <li
                onClick={() => addFrise(timeline)}
                key={index}
                className={"timeline"}
              >
                {timeline}
              </li>
            );
          })}
        </ul>
      ) : null}
    </div>
  );
};

export default SearchBarHome;
