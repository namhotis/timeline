import React from 'react'
import './index.scss'

const AddTimelineTrigger = ({
  isVisibleAddTimelineContainer,
  setIsVisibleAddTimelineContainer,
}) => {
  return <button className="addTimelineTrigger" onClick={() => setIsVisibleAddTimelineContainer(!isVisibleAddTimelineContainer)}>+</button>;
};

export default AddTimelineTrigger
