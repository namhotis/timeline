import React from 'react'
import { Link } from 'react-router-dom'
import './index.scss'

const LinkList = (
    { linkList }
  ) => {
  return (
    <ul className="listContainer">    
        {linkList.map((link) => (
            <Link key={link} to="/">
                <li>{link}</li>
            </Link>
        ))}
    </ul>
  );
}

export default LinkList