import React, { useState, useEffect } from 'react'
import arrays from '../../../../tools/arrays'
import './index.scss'

const MainPeriod = ({
  formatedEndingDateGrid,
  formatedStartingDateGrid,
  timelineEnd,
  timelineStart
}) => {
  const [xPosStart, setXPosStart] = useState(0);
  const [xPosEnd, setXPosEnd] = useState(0);

  useEffect(() => {
    if (
      !arrays.isEmpty(formatedStartingDateGrid) &&
      !arrays.isEmpty(formatedEndingDateGrid)
    ) {
      const totalYear = formatedEndingDateGrid - formatedStartingDateGrid;
      const percentYear = totalYear / 100;
      const startNumberYearsDifferenceFromStart =
        timelineStart.year - formatedStartingDateGrid;
      const endNumberYearsDifferenceFromStart =
        timelineEnd.year - formatedStartingDateGrid;
      setXPosStart(startNumberYearsDifferenceFromStart / percentYear);
      setXPosEnd(endNumberYearsDifferenceFromStart / percentYear);

      console.log("percentYear", percentYear);
    }

    console.log(
      "XPOSStart",
      xPosStart,
      xPosEnd,
      formatedEndingDateGrid,
      formatedStartingDateGrid,
      timelineStart,
      timelineEnd
    );
  });

  return (
    <div
      style={{
        left: xPosStart + "%",
        right: 100 - xPosEnd + "%",
      }}
      className="mainPeriod"
    ></div>
  );
};

export default MainPeriod
