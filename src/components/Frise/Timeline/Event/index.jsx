import React, { useEffect, useState } from "react";
import arrays from "../../../../tools/arrays";
import date from "../../../../tools/date";
import "moment/locale/fr";
import moment from "moment";
import './index.scss'

const Event = ({
  event,
  formatedEndingDateGrid,
  formatedStartingDateGrid
}) => {
  const [xPosStart, setXPosStart] = useState(0);

  useEffect(() => {
    if (
      !arrays.isEmpty(formatedStartingDateGrid) &&
      !arrays.isEmpty(formatedEndingDateGrid)
    ) {
      const totalYear = formatedEndingDateGrid - formatedStartingDateGrid;
      const percentYear = totalYear / 100;
      const startNumberYearsDifferenceFromStart =
        date.formatDate(event.date).year - formatedStartingDateGrid;
      setXPosStart(startNumberYearsDifferenceFromStart / percentYear);
    }
  });
  return (
    <div
      style={{
        left: xPosStart + "%",
      }}
      className="timelineEvent"
    >
      <p>{event.title}</p>
      <p className="eventDate">{moment(event.date).locale('fr').format("ll")}</p>
    </div>
  );
};

export default Event
