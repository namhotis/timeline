import React, { useState, useEffect } from 'react'
import './index.scss'
import MainPeriod from './MainPeriod'
import date from "../../../tools/date";
import TimelinePresentation, {
  LoadingTimeline,
 } from "../../ToAddTimelines/TimelinePresentation";
import { TimelinesContext } from '../../../contexts/TimelinesContext'
// import Event from './Event';
import { selectTimeline, updateTimeline } from "../../../contexts/TimelinesReducer";
import youtubeapi from './../youtubeapi'
import arrays from '../../../tools/arrays';
import Event from './Event';
import Period from './Period';
// import Period from './Period';

const DEFAULT_WIKIPEDIA_IMAGE_AND_INTRO =
  "https://fr.wikipedia.org/w/api.php?format=json&pithumbsize=200&action=query&origin=*&prop=pageimages&titles=";

const Timeline = ({
  formatedEndingDateGrid,
  formatedStartingDateGrid,
  timeline,
  setComponentState,
  friseState,
}) => {
  const [wiki, setWiki] = useState({})
  const [ytVideos, setYtVideos] = useState({});

  const timelinesGlobalState = React.useContext(TimelinesContext);

  const [timelineState, setTimelineState] = useState({
    isLoading: false,
    title: null,
    events: [],
    endPeriod: null,
    startPeriod: null,
    wiki: null,
    ytVideos: null,
  });

  React.useEffect(() => {
    console.log("Timeline, timeline", timeline)
  }, [timeline])

  const [presentationImage, setPresentationImage] = useState(null);

  useEffect(() => {
    fetch(
      `${DEFAULT_WIKIPEDIA_IMAGE_AND_INTRO}${encodeURIComponent(
        timeline.title
      )}`
    )
      .then((response) => response.json())
      .then((response) => {
        // Trick to get the name of the children of the object and access it without knowing it
        const pageInfos =
          response.query.pages[
            Object.getOwnPropertyNames(response.query.pages)[0]
          ];

        setPresentationImage(pageInfos.thumbnail.source);

        timeline.presentationImage = pageInfos.thumbnail.source;

        timelinesGlobalState.dispatch(updateTimeline(timeline));
      })
      .catch((error) => console.error(error));
  }, []);

  useEffect(() => {
    if (timelineState.title && (wiki || ytVideos)) {
      setTimelineState({
        ...timelineState,
        wiki,
        ytVideos,
      });
    }
  }, [wiki, ytVideos, timelineState.title])

  useEffect(() => {
    console.log("Timeline State", timelineState);
    timelinesGlobalState.dispatch(updateTimeline(timelineState));
  }, [timelineState])

  // On récupère les informations stockées sur la page wikipedia afin d'avoir la description d'une frise
  const getWiki = (term) => {
    // TODO : keep all events in a redux store to prevent to many calls
    const DEFAULT_WIKIPEDIA_IMAGE_AND_INTRO =
      "https://fr.wikipedia.org/w/api.php?format=json&pithumbsize=200&action=query&origin=*&prop=pageimages|extracts&exintro=1&titles=";
    fetch(`${DEFAULT_WIKIPEDIA_IMAGE_AND_INTRO}${encodeURIComponent(term)}`)
      .then((response) => response.json())
      .then((response) => {
        // Trick to get the name of the children of the object and access it without knowing it
        const pageInfos =
          response.query.pages[
            Object.getOwnPropertyNames(response.query.pages)[0]
          ];

        // const _timelineState = {
        //   ...timelineState,
        //   wiki: pageInfos,
        // };

        // setTimelineState(_timelineState);

        setWiki(pageInfos)
      });
  };

  const getYoutubeVideos = async (term) => {
    const response = await youtubeapi.get("/search", {
      params: {
        q: term,
      },
    });

    // const _timelineState = {
    //   ...timelineState,
    //   ytVideos: response.data,
    // };

    setYtVideos(response.data);
    // setTimelineState(_timelineState);
  };

  const getTimelineInfos = (timeline) => {
    getWiki(timeline.title);
    getYoutubeVideos(timeline.title);
    setComponentState({ ...friseState, isOpenInfoBar: true });
  };

  // On récupère tous les articles ayant le nom souhaité
  const getExistingArticles = () => {
    fetch(`${process.env.REACT_APP_API_BACK_URL}/search/${timeline.title}`)
      .then((response) => response.json())
      .then((response) => {
        console.log("Response", response)

        console.log(
          arrays.sortByPertinence(arrays.onlyExact(response, timeline.title))[0].decrit_par
        );
        // On récupère le résultat le plus pertinent
        // La pertinence d'un résultat est calculée en fonction du nombre de clefs d'information
        response &&
          buildTimeline(
            arrays.sortByPertinence(
              arrays.onlyExact(response, timeline.title)
            )[0]
          );
      })
      
      .catch((err) => console.error("GetExistingArticles", err));
  };

  const buildTimeline = (info) => {
    console.log("Build Timeline", info);

    let startingDate;
    let endingDate;
    let events = [];

    // Start
    if (info.date_de_naissance) {
      if (typeof info.date_de_naissance === "object") {
        events.push({
          id: events.length,
          title: "Naissance",
          date: new Date(info.date_de_naissance[0]),
        });
        startingDate = new Date(info.date_de_naissance[0]);
      } else {
         events.push({
           id: events.length,
           title: "Naissance",
           date: new Date(info.date_de_naissance),
         });
        startingDate = new Date(info.date_de_naissance);
      }
    } else if (info.date_de_debut) {
       events.push({
         id: events.length,
         title: "Création",
         date: new Date(info.date_de_debut),
       });
      startingDate = new Date(info.date_de_debut);
    } else {
      console.log("L'instance n'est pas une période ou un personnage");
    }

    // End
    if (info.date_de_mort) {
      if (typeof info.date_de_mort === "object") {
       events.push({
         id: events.length,
         title: "Décès",
         date: new Date(info.date_de_debut),
       });
        endingDate = new Date(info.date_de_mort[0]);
      } else if (typeof info.date_de_mort === "string") {
       events.push({
         id: events.length,
         title: "Décès",
         date: new Date(info.date_de_mort),
       });
        endingDate = new Date(info.date_de_mort);
      }
    } else if (info.date_de_fin) {
      if (typeof info.date_de_fin === "object") {
       events.push({
         id: events.length,
         title: "Décès",
         date: new Date(info.date_de_fin[0]),
       });
        endingDate = new Date(info.date_de_fin[0]);
      } else if (typeof info.date_de_fin === "string") {
       events.push({
         id: events.length,
         title: "Décès",
         date: new Date(info.date_de_fin),
       });
        endingDate = new Date(info.date_de_fin);
      }
    } else {
      console.log("L'instance n'a pas de date de fin ou de mort");

      if (info.date_de_naissance && !info.date_de_mort) {
        endingDate = null;
        startingDate = null;

        events.push({
          id: events.length,
          title: "Naissance",
          date: new Date(info.date_de_naissance),
        });
      }
    }

    const timeline = {
      title: info.label,
      startingDate: new Date(startingDate),
      endingDate: new Date(endingDate),
      events: events,
      isLoading: false,
      presentationImage,
      info,
      wiki,
      ytVideos
    };

    // const _currentTimelines = timelinesGlobalState.state.currentTimelines;
    // _currentTimelines.push(timeline);

    setTimelineState(timeline)
    // timelinesGlobalState.state.currentTimelines = _currentTimelines;
    timelinesGlobalState.dispatch(updateTimeline(timeline));
    timelinesGlobalState.dispatch(selectTimeline(timeline));
  };

  // Get new infos in infobar if add a Timeline
  useEffect(() => {
    // TODO : SET CURRENT TIMELINE
    // timelineBuilderState.currentTimeline = timeline;
    if (timeline.isLoading === true) {
      getExistingArticles();
      getTimelineInfos(timeline);
    }
  }, []);

  if (timeline.isLoading === true) {
    return (
      <LoadingTimeline
        key={timeline.title}
        title={timeline.title}
        showTitle
        loading
        presentationImage={presentationImage}
        timeline={timeline}
      />
    );
  } else {
    return (
      <section className="timeline">
        <TimelinePresentation
          anim
          onClick={() =>
            timelinesGlobalState.dispatch(selectTimeline(timeline))
          }
          showTitle
          presentationImage={presentationImage}
          timeline={timeline}
        />
        <MainPeriod
          formatedEndingDateGrid={formatedEndingDateGrid}
          formatedStartingDateGrid={formatedStartingDateGrid}
          timeline={timelineState}
          timelineStart={date.formatDate(timelineState.startingDate) || null}
          timelineEnd={date.formatDate(timelineState.endingDate) || null}
        />
        {timeline.events.map((event, index) => {
          if (event.date) {
            // return event.active ? (
            //   <Event
            //     key={index}
            //     formatedEndingDateGrid={formatedEndingDateGrid}
            //     formatedStartingDateGrid={formatedStartingDateGrid}
            //     timeline={timeline}
            //     event={event}
            //   />
            // ) : null;
            return (
              <Event
                key={index}
                formatedEndingDateGrid={formatedEndingDateGrid}
                formatedStartingDateGrid={formatedStartingDateGrid}
                timeline={timeline}
                event={event}
              />
            )
          } else if (event.startingDate && event.endingDate) {
            return event.active ? (
              <Period
                key={index}
                formatedEndingDateGrid={formatedEndingDateGrid}
                formatedStartingDateGrid={formatedStartingDateGrid}
                timeline={timeline}
                event={event}
                timelineStart={date.formatDate(event.startingDate) || null}
                timelineEnd={date.formatDate(event.endingDate) || null}
              />
            ) : null;
          }
        })}
      </section>
    );
  }
};

export default Timeline
