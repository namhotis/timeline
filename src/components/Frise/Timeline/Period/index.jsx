import React, { useEffect, useState } from "react";
import arrays from "../../../../tools/arrays";
import "./index.scss";

const Period = ({
  formatedEndingDateGrid,
  formatedStartingDateGrid,
  timelineEnd,
  timelineStart,
  event
}) => {
  const [xPosStart, setXPosStart] = useState(0);
  const [xPosEnd, setXPosEnd] = useState(0);

  useEffect(() => {
    if (
      !arrays.isEmpty(formatedStartingDateGrid) &&
      !arrays.isEmpty(formatedEndingDateGrid)
    ) {
      const totalYear = formatedEndingDateGrid - formatedStartingDateGrid;
      const percentYear = totalYear / 100;
      const startNumberYearsDifferenceFromStart =
        timelineStart.year - formatedStartingDateGrid;
      const endNumberYearsDifferenceFromStart =
        timelineEnd.year - formatedStartingDateGrid;
      setXPosStart(startNumberYearsDifferenceFromStart / percentYear);
      setXPosEnd(endNumberYearsDifferenceFromStart / percentYear);
    }
  });

  return (
    <div
      style={{
        left: xPosStart + "%",
        right: 100 - xPosEnd + "%",
      }}
      className="period"
    >
      <p>{event.title}</p>
    </div>
  );
};

export default Period;
