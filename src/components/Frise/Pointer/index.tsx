import React from 'react'
import './index.scss'

type PointerProps = {
  pointerX: number
};

const Pointer = ({ pointerX }: PointerProps) => {
  return (
    <div
      style={{
        left: pointerX,
      }}
      className="pointer"
    ></div>
  );
};

export default Pointer
