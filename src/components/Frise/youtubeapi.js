import axios from 'axios'

export default axios.create({
  baseURL: `https://www.googleapis.com/youtube/v3/`,
  params: {
    part: "snippet",
    maxResult: 10,
    key: process.env.REACT_APP_YT_API_KEY
  },
});
