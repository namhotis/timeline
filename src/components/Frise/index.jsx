import React, { useEffect, useState, useRef, useContext } from 'react'
import AddTimelineContainer from "../AddTimelineContainer"
import AddTimelineTrigger from '../AddTimelineTrigger'
import TimelineInfoBar from "./TimelineInfoBar";
import Timeline from './Timeline'
import Grille from "./Grille";
import Pointer from './Pointer'
import date from '../../tools/date'
import arrays from '../../tools/arrays'
import './index.scss'
import { TimelinesContext } from "../../contexts/TimelinesContext";
// import { EventsContext } from "../../contexts/EventsContext";

Object.size = function (obj) {
  var size = 0,
    key;
  for (key in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

const Frise = () => {
  const frise = useRef(null);
  const timelinesGlobalState = useContext(TimelinesContext);
  const [
    isVisibleAddTimelineContainer,
    setIsVisibleAddTimelineContainer,
  ] = useState(false);
  const [ytVideos, setYtVideos] = useState([]);
  const [componentState, setComponentState] = useState({
    isLoading: false,
    startingDate: null,
    endingDate: null,
    isOpenInfoBar: false,
  });
  const [formatedStartingDate, setFormatedStartingDate] = useState({});
  const [formatedEndingDate, setFormatedEndingDate] = useState({});
  const [eWheel, setEWheel] = useState();
  const [formatedStartingDateGrid, setFormatedStartingDateGrid] = useState(
    null
  );
  const [formatedEndingDateGrid, setFormatedEndingDateGrid] = useState(null);
  const [pointerX, setPointerX] = useState(0);
  const openPopup = () => setIsVisibleAddTimelineContainer(true);
  const closePopup = () => setIsVisibleAddTimelineContainer(false);

  const initFris = (timeline) => {

    let startingTime = null;
    let endingTime = null;
    console.log("Timeline", timeline)

    if (timeline.startingDate && timeline.startingDate !== null) {
        timeline.startingDate = new Date(timeline.startingDate);

        if (componentState.startingDate === null) {
          startingTime = new Date(timeline.startingDate);
        } 
        
        else if (new Date(timeline.startingDate) < new Date(componentState.startingDate)) {
          startingTime = timeline.startingDate;
        }
      }

      if (timeline.endingDate && timeline.endingDate !== null) {
        timeline.endingDate = new Date(timeline.endingDate);
        if (
          date.getEndingDate(
            timeline.endingDate,
            componentState.endingDate
          ) === true
        ) {
          endingTime = new Date(timeline.endingDate);
        }
      }

      if (timeline.endingDate && timeline.endingDate === null) {
        timeline.endingDate = new Date()
        if (
          date.getEndingDate(
            timeline.endingDate,
            componentState.endingDate
          ) === true
        ) {
          endingTime = new Date(timeline.endingDate);
        }
        timeline.isEnded = false
      }

      console.log("Starting, ending", startingTime, endingTime)
      setComponentState({ ...componentState, isLoading: false, startingDate: startingTime, endingDate: endingTime })
  };

  const addTimelines = (toAddTimelines) => {
    timelinesGlobalState.state.currentTimelines = arrays.getUniquesValueFromArray(
      [...timelinesGlobalState.state.currentTimelines, ...toAddTimelines]
    );
    closePopup();
  };

  // Get all the timelines from the static file
  useEffect(() => {
    frise?.current?.addEventListener("wheel", (e) => setEWheel(e.deltaY));
    frise?.current?.addEventListener("mousemove", (e) =>
      setPointerX(e.clientX)
    );
    return () => {
      frise?.current?.removeEventListener("wheel", (e) => setEWheel(e.deltaY));
      frise?.current?.removeEventListener("mousemove", (e) =>
        setPointerX(e.clientX)
      );
    };
  }, []);

  // Lorsque l'utilisateur scroll, la grille change de date
  const resizeGridOnWheel = () => {
    if (
      !arrays.isEmpty(formatedStartingDate) &&
      !arrays.isEmpty(formatedEndingDate)
    ) {
      setFormatedStartingDate({
        ...formatedStartingDate,
        year: formatedStartingDate.year - Math.floor(eWheel / 4),
      });

      setFormatedEndingDate({
        ...formatedEndingDate,
        year: formatedEndingDate.year - Math.floor(eWheel / 4),
      });
    }
  };

  // Wheel Handler
  useEffect(() => resizeGridOnWheel(), [eWheel]);

  useEffect(() => {
    timelinesGlobalState.state.currentTimeline !== null &&
      initFris(
        timelinesGlobalState.state.currentTimelines.find(
          (timeline) =>
            (timeline.id === timelinesGlobalState.state.currentTimeline)
        )
      );
  }, [timelinesGlobalState.state.currentTimeline]);

  // TODO : Format this to get less var (startingDate && endingDate must come directly formatted)
  useEffect(() => {
    componentState.startingDate &&
      componentState.startingDate !== null &&
      setFormatedStartingDate(date.formatDate(componentState.startingDate));
    componentState.endingDate &&
      componentState.endingDate !== null &&
      setFormatedEndingDate(date.formatDate(componentState.endingDate));
  }, [componentState.startingDate, componentState.endingDate]);

  // const deleteTimeline = (id) => {
  // setCurrentTimelines(
  //   currentTimelines.filter((timeline) => timeline.id !== id)
  // );
  // };

  return (
    <section className="frise" ref={frise}>
      <Pointer pointerX={pointerX} />
      {/* TODO : Réduire le nombre de props */}
      {isVisibleAddTimelineContainer ? (
        <AddTimelineContainer
          addTimelines={addTimelines}
          currentTimelines={timelinesGlobalState.currentTimelines}
          openPopup={openPopup}
          closePopup={closePopup}
          isVisibleAddTimelineContainer={isVisibleAddTimelineContainer}
          setIsVisibleAddTimelineContainer={setIsVisibleAddTimelineContainer}
        />
      ) : null}
      <section className="currentFrisesContainer">
        {timelinesGlobalState.state.currentTimelines.map((timeline, index) => (
          <Timeline
            formatedStartingDateGrid={formatedStartingDateGrid}
            formatedEndingDateGrid={formatedEndingDateGrid}
            formatedStartingDate={formatedStartingDate}
            formatedEndingDate={formatedEndingDate}
            setComponentState={setComponentState}
            key={index}
            setYtVideos={setYtVideos}
            timeline={timeline}
            friseState={componentState}
            setFriseState={setComponentState}
          />
        ))}

        <AddTimelineTrigger
          isVisibleAddTimelineContainer={isVisibleAddTimelineContainer}
          setIsVisibleAddTimelineContainer={setIsVisibleAddTimelineContainer}
        />
      </section>
      <TimelineInfoBar
        ytVideos={ytVideos}
        componentState={componentState}
        setComponentState={setComponentState}
        currentTimelines={timelinesGlobalState.currentTimelines}
      />
      <Grille
        setFormatedStartingDateGrid={setFormatedStartingDateGrid}
        setFormatedEndingDateGrid={setFormatedEndingDateGrid}
        formatedStartingDate={formatedStartingDate}
        formatedEndingDate={formatedEndingDate}
      />
    </section>
  );
};

export default Frise
