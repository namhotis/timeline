import React from 'react'
import './index.scss'

const YTVideosList = ({ ytVideos }) => {
  
  React.useEffect(() => {
    console.log("YTVideos", ytVideos);
  }, [ytVideos]);

  return (
    <section className="YTVideoList">
      {ytVideos &&
        ytVideos.items &&
        ytVideos.items.map(
          (video) =>
            video &&
            video.snippet && (
              <a
                href={`https://www.youtube.com/watch?v=${video.id.videoId}`}
                key={video.id.videoId}
                className="video-item"
                target="_blank"
                rel="noreferrer"
              >
                <img src={video.snippet.thumbnails.default.url} alt="" />
                <div className="video-infos">
                  <h2>{video.snippet.title}</h2>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href={`https://www.youtube.com/channel/${video.snippet.channelId}`}
                  >
                    {video.snippet.channelTitle}
                  </a>
                  <p>{video.snippet.description}</p>
                </div>
              </a>
            )
        )}
    </section>
  );
};

export default YTVideosList
