import React, { useState } from "react";
import YTVideosList from '../YTVideosList'
import EventsList from '../EventsList'
import TimelineAbout from "../TimelineAbout";
import './index.scss'
import { TimelinesContext } from "../../../contexts/TimelinesContext";

const TimelineInfoBar = ({
  deleteTimeline,
  componentState,
  setComponentState
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const timelinesGlobalState = React.useContext(TimelinesContext)

  const [timeline, setTimeline] = useState({})

  React.useEffect(() => {
    if (timelinesGlobalState?.state?.currentTimelines.length > 0) {
      setTimeline(
        timelinesGlobalState?.state?.currentTimelines.find(
          (timeline) =>
            timeline.id === timelinesGlobalState?.state?.currentTimeline
        )
      );
    }
  }, [timelinesGlobalState.state.currentTimeline]);

  return (
    <aside
      className={`timeline-info-bar ${
        componentState.isOpenInfoBar ? "open" : "close"
      }`}
    >
      <button
        className="closeTimelineInfoBar"
        onClick={() =>
          setComponentState({ ...componentState, isOpenInfoBar: false })
        }
      >
        x
      </button>
      <div className="flex row wikiDetails">
        {timeline?.wiki?.thumbnail && (
          <img src={timeline.wiki.thumbnail.source} alt="" />
        )}
        <div className="wikiDetailsTexts">
          {(timeline?.wiki?.title && <h2>{timeline?.wiki.title}</h2>) ||
            <div className="loaderWikiDetails"></div>}
        </div>
      </div>
      <ul>
        <li
          className={`${currentIndex === 0 ? "active" : null}`}
          onClick={() => setCurrentIndex(0)}
        >
          À propos
        </li>
        <li
          className={`${currentIndex === 1 ? "active" : null}`}
          onClick={() => setCurrentIndex(1)}
        >
          Événements
        </li>
        <li
          className={`${currentIndex === 2 ? "active" : null}`}
          onClick={() => setCurrentIndex(2)}
        >
          Vidéos
        </li>
      </ul>

      {currentIndex === 0 && (
        <TimelineAbout
          deleteTimeline={deleteTimeline}
          wiki={timeline?.wiki}
          wikiText={timeline?.wiki?.extract.substring(0, 1500)}
        />
      )}
      {currentIndex === 1 && <EventsList events={timeline.events} />}
      {currentIndex === 2 && <YTVideosList ytVideos={timeline.ytVideos} />}
    </aside>
  );
};

export default TimelineInfoBar
