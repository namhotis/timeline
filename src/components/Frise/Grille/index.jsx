import React from 'react';
 import { useEffect } from 'react';
 import { useRef } from 'react';
 import arrays from '../../../tools/arrays'
 
const getPixelRatio = context => {
    var backingStore =
    context.backingStorePixelRatio ||
    context.webkitBackingStorePixelRatio ||
    context.mozBackingStorePixelRatio ||
    context.msBackingStorePixelRatio ||
    context.oBackingStorePixelRatio ||
    context.backingStorePixelRatio ||
    1;
    
    return (window.devicePixelRatio || 1) / backingStore;
};
 
 const Grille = ({
   formatedStartingDate,
   formatedEndingDate,
   setFormatedStartingDateGrid,
   setFormatedEndingDateGrid,
 }) => {
   let ref = useRef();

   const drawGrid = () => {
     let canvas = ref.current;
     let ctx = canvas.getContext("2d");

     let ratio = getPixelRatio(ctx);
     let width = getComputedStyle(canvas)
       .getPropertyValue("width")
       .slice(0, -2);
     let height = getComputedStyle(canvas)
       .getPropertyValue("height")
       .slice(0, -2);

     canvas.width = width * ratio;
     canvas.height = height * ratio;
      ctx.scale(1/ratio*2, 1/ratio*2);
     canvas.style.width = `${width}px`;
     canvas.style.height = `${height}px`;

     ctx.beginPath();
     ctx.lineWidth = 2;
     ctx.moveTo(1, 100);
     ctx.lineTo(1, 70);

     //  Draw lines
     if (
       !arrays.isEmpty(formatedStartingDate) &&
       !arrays.isEmpty(formatedEndingDate)
     ) {
       const yearInsideGrid = Math.abs(
         formatedEndingDate.year - formatedStartingDate.year
       );

        setFormatedStartingDateGrid(formatedStartingDate.year);
        setFormatedEndingDateGrid(formatedEndingDate.year);
       const numberOfSlides = yearInsideGrid / 5;

       // On dessine tous les 5 ans
       for (let i = 0; i < numberOfSlides + 1; i++) {
         ctx.moveTo(i * (canvas.width / numberOfSlides), 100);
         ctx.lineTo(i * (canvas.width / numberOfSlides), 70);

         ctx.font = "30px Arial";
         ctx.textAlign = "center";
         ctx.fillText(
           `${formatedStartingDate.year + i * 5}`,
           i * (canvas.width / numberOfSlides),
           50
         );
       }

       // On dessine chaque année
       for (let i = 0; i < numberOfSlides * 5; i++) {
         ctx.moveTo(i * (canvas.width / (numberOfSlides * 5)), 100);
         ctx.lineTo(i * (canvas.width / (numberOfSlides * 5)), 90);
       }
     }
     ctx.stroke();
   };

   useEffect(() => {
     drawGrid();
   }, [formatedEndingDate, formatedStartingDate]);

   useEffect(() => {
     window.addEventListener("resize", drawGrid);

     return () => window.removeEventListener("resize", drawGrid);
   }, []);

   return (
     <canvas
       ref={ref}
       style={{ width: "calc(100% - 50px)", height: "50px", left: "50px" }}
     />
   );
 };
 
 export default Grille;
