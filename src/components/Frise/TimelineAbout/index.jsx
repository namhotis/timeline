import React from 'react'
import './index.scss'

const TimelineAbout = ({ wiki, wikiText, currentTimeline, deleteTimeline }) => {
  return (
    <section className="timelineAbout flex column">
      <div className="wikiExtract">
        <h3>History (Wikipedia)</h3>
        <hr />
        {wiki && wiki?.extract && (
          <div
            className="wikiExtractText"
            dangerouslySetInnerHTML={{
              __html: wikiText + `<a target="_blank" href="https://fr.wikipedia.org/wiki/${
          wiki.title
        }">Lire plus</a>`,
            }}
          />
        ) || <div className="loaderWikiExtract"></div>}
      </div>
      <button
        onClick={() => deleteTimeline(currentTimeline.id)}
        className="deleteTimeline-btn"
      >
        Supprimer la timeline
      </button>
    </section>
  );
};

export default TimelineAbout
