import React, { useState } from 'react'
import "./index.scss"

const EventCheckbox = ({ event }) => {
  const [checked, setChecked] = useState(event.active);

  return (
    <div key={event.id}>
      <input
        type="checkbox"
        id={event.title}
        checked={checked === true ? true : false}
        name={event.title}
        onChange={(e) => setChecked(e.target.checked)}
      />
      <label htmlFor={event.title}>{event.title}</label>
    </div>
  );
};

const EventsList = ({ events }) => {
  // const [currentDetailedEvents, setCurrentDetailedEvents] = useState([]);

  // useEffect(() => {
  //   const tmpDetailedEvents = [];
  //   detailedEvents.forEach((event) => {
  //     if (currentTimeline.events.includes(event.id)) {
  //       tmpDetailedEvents.push(event);
  //     }
  //   })
    
  //   setCurrentDetailedEvents(tmpDetailedEvents);
  // }, [currentTimeline, detailedEvents]);

  return (
    <aside className="eventsList">
      {events.map((event) => (
        <EventCheckbox
          key={event.id}
          event={event}
        />
      ))}
    </aside>
  );
};

export default EventsList
