import React, { useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { loginUser, useAuthDispatch, useAuthState } from '../../contexts/UserContext'
import './index.scss'

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  let history = useHistory();

  const state = useAuthState();
  const dispatch = useAuthDispatch(); //get the dispatch method from the useDispatch custom hook

  const handleLogin = async (e) => {
    console.log(state);
    e.preventDefault();
    let payload = { email, password };
    try {
      let response = await loginUser(dispatch, payload); //loginUser action makes the request and handles all the neccessary state changes
      if (!response.user) return;
      history.push("/timeline"); //navigate to dashboard on success
    } catch (error) {
      console.log(error)
    }
  };

  return (
    <div>
      <div className="authenticateContainer">
        <div className="authenticateContainerHeader">
          <h1>Connexion</h1>
          <p>Vous n&apos;avez pas de compte ? <Link to="/register">S&apos;inscrire</Link></p>
        </div>
        <form>
          <div className={"loginForm"}>
            <div>
              <img src={`/assets/img/icon-person.svg`} />
              <div className={"loginFormItem"}>
                <input
                  type="text"
                  id="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Email"
                />
                <div className="border-field"></div>
              </div>
            </div>
            <div>
              <img src={`/assets/img/icon-lock.svg`} />
              <div className={"loginFormItem"}>
                <input
                  type="password"
                  id="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="Mot de passe"
                />
                <div className="border-field"></div>
              </div>
            </div>
            {/* <p className="lostPassword">Mot de passe perdu ?</p> */}
          </div>
          <button onClick={handleLogin}>Connexion</button>
        </form>
      </div>
    </div>
  );
}

export default Login
