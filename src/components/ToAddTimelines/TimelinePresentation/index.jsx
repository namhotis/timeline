import React from 'react'
import './index.scss'

const TimelinePresentation = ({ timeline, showTitle, bold, onClick, anim, presentationImage }) => {
  return (
    <li className="timeline-presentation-container">
      <img
        onClick={onClick}
        src={presentationImage}
        className={`timeline-presentation-image ${anim ? "anim" : ""} ${
          onClick ? "clickable" : ""
        }`}
        alt={``}
      />
      {showTitle && timeline?.title && (
        <p className={`${bold ? "bold" : ""}`}>{timeline.title}</p>
      )}
    </li>
  );
}

export const LoadingTimeline = ({ title, showTitle, bold, onClick, anim, loading, presentationImage }) => {
  return (
    <li className="timeline-presentation-container">
      <div
        onClick={onClick}
        className={`timeline-presentation-image ${anim ? "anim" : ""} ${
          onClick ? "clickable" : ""
        } ${loading ? "loading" : ""}`}
        alt={``}
      >
        <div
          className="lds-ripple"
          style={{ backgroundImage: `url('${presentationImage}')` }}
        >
          <div></div>
          <div></div>
        </div>
      </div>
      {showTitle && title && <p className={`${bold ? "bold" : ""}`}>{title}</p>}
    </li>
  );
}

export default TimelinePresentation
