import React from 'react'
import TimelinePresentation from './TimelinePresentation'
import './index.scss'

const ToAddTimelines = ({ toAddTimelines }) => {
  return (
    <ul>
      {toAddTimelines.map((timeline) => (
        <TimelinePresentation bold={false} showTitle key={timeline.id} timeline={timeline} />
      ))}
    </ul>
  );
};

export default ToAddTimelines
