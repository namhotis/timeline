import React, { useEffect, useState, useRef } from 'react'
import './index.scss'

const SearchBar = () => {
  const [searchBarValue, setSearchBarValue] = useState("");
  const [showProposedTimelines, setShowProposedTimelines] = useState(true);
  const inputEl = useRef(null);

  useEffect(() => {
    inputEl.current.focus();
  }, []);

  useEffect(() => {
    console.log(showProposedTimelines);
  }, [searchBarValue]);

  return (
    <div className="searchbar">
      <input
        type="text"
        value={searchBarValue}
        onChange={(e) => setSearchBarValue(e.target.value)}
        ref={inputEl}
        onFocus={() => setShowProposedTimelines(true)}
      />
      {showProposedTimelines ? (
        <ul className="search-bar-results">
        </ul>
      ) : null}
    </div>
  );
};

export default SearchBar
