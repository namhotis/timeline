import React from 'react'
import './index.scss'
import TimelinePresentation from '../ToAddTimelines/TimelinePresentation'
import SearchBarHome from '../SearchBarHome'

import { TimelinesContext } from "../../contexts/TimelinesContext";

const AddTimelineContainer = ({
  isVisibleAddTimelineContainer,
  setIsVisibleAddTimelineContainer,
  // addTimelines,
  closePopup,
}) => {
  const timelinesGlobalState = React.useContext(TimelinesContext);

  React.useState(() => {
    console.log("timelinesGlobalState", timelinesGlobalState);
  }, [timelinesGlobalState.timelines]);
  // const validateButton = useRef(null);

  // Si la timeline n'est pas déjà dans les timelines proposées, on l'ajoute

  return isVisibleAddTimelineContainer ? (
    <section className="addTimelineContainer">
      <div
        onClick={() =>
          setIsVisibleAddTimelineContainer(!isVisibleAddTimelineContainer)
        }
        className="toggleVisibleTimelineFilter"
      ></div>
      <section className="addTimeline">
        <header>
          <h1>Nouvelle frise</h1>
          {/* Close AddTimelineContainer */}
          <button onClick={closePopup}>x</button>
        </header>
        <div className="addTimelineContainerContent">
          <section>
            <SearchBarHome />
            {/* searchbar and wanted to add frises */}
          </section>
          <section className="currentTimelines">
            {/* {currentTimelines.map((timeline) => (
              <p key={timeline.id}>{timeline.title}</p>
            ))} */}
            <ul>
              {timelinesGlobalState.state.currentTimelines.map((timeline, index) => (
                <TimelinePresentation
                  bold={false}
                  showTitle
                  key={index}
                  timeline={timeline}
                  presentationImage={timeline.presentationImage}
                />
              ))}
            </ul>
          </section>
        </div>
      </section>
    </section>
  ) : null;
};

export default AddTimelineContainer
