import React from 'react'
import FullLogo from '../FullLogo'
import './index.scss'

const Footer = () => {
  return (
    <div className="footer">
        <FullLogo />
        <p className="waterMark">© 2021</p>
    </div>
  );
}

export default Footer