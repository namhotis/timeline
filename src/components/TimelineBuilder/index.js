import React from 'react'
import './index.scss'

import Header from '../Header'
import Frise from '../Frise'

const TimelineBuilder = () => {
  return (
    <main className="TimelineBuilder">
      <Header mainLogo={true} />
      <Frise />
      {/* {loading && <p>loading...</p>}
      {error && <p>Les timelines n&apos;ont pas pu être trouvées</p>} */}
      <div className="handleTimelinesContainer"></div>
    </main>
  );
};

export default TimelineBuilder;
