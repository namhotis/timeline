import React from 'react'
import { Link } from 'react-router-dom'
import './index.scss'

const MainLogo = () => {
  return (
    <Link className="main-logo-container" to="/">
      <img src={`/assets/img/logo-timeline.svg`} className="main-logo" alt="" />
    </Link>
  );
}

export default MainLogo
