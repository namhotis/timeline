import React from 'react'
import { Link } from 'react-router-dom'
import './index.scss'

const FullLogo = () => {
  return (
    <Link className="main-logo-container" to="/">
      <img src={`/assets/img/logo-timeline2.svg`} className="main-logo" alt="" />
    </Link>
  );
}

export default FullLogo
