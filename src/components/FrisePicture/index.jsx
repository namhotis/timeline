import React from "react";
import { useEffect, useState } from 'react'
import './index.scss'

const DEFAULT_WIKIPEDIA_IMAGE_AND_INTRO = "https://fr.wikipedia.org/w/api.php?format=json&action=query&pithumbsize=200&origin=*&prop=pageimages|extracts&exintro=1&titles=";

const FrisePicture = ({ title }) => {
  const [profilePicture, setProfilePicture] = useState(null)
  const [articleIntroductionText, setArticleIntroductionText] = useState('')

  useEffect(() => {
    const WIKIPEDIA_API_URL = `${DEFAULT_WIKIPEDIA_IMAGE_AND_INTRO}${encodeURIComponent(title)}`

    fetch(WIKIPEDIA_API_URL)
      .then((response) => response.json())
      .then((response) => {
        // Trick to get the name of the children of the object and access it without knowing it
        const pageInfos =
          response.query.pages[
            Object.getOwnPropertyNames(response.query.pages)[0]
          ];
        setProfilePicture(pageInfos.thumbnail.source);
        setArticleIntroductionText(pageInfos.extract)
      }
      )
      .catch((error) => console.error(error));
  }, [])

  return(
    <div className="FrisePicture">
      {/* {title} */}
      <img className="profilePictureBubble" src={profilePicture} alt="" />
      <p dangerouslySetInnerHTML={{ __html: articleIntroductionText }} />
    </div>
  )
}

export default FrisePicture
