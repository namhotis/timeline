import React from 'react'
import FrisePicture from '../FrisePicture'
import './index.scss'

const FrisePictureContainer = ({ visibleFrises, response }) => {
  return (
    <ul className="frisePictureContainer">
      {visibleFrises.map((visibleCategorie) => {
        return (
          <li key={visibleCategorie}>
            <FrisePicture
              title={
                response.find((Frise) => Frise.id === visibleCategorie).title
              }
            />
          </li>
        );
      })}
    </ul>
  );
};

export default FrisePictureContainer
