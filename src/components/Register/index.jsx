import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { registerUser, useAuthDispatch } from '../../contexts/UserContext'
import { Link } from 'react-router-dom'
import '../Login/index.scss'

function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  let history = useHistory();

  const dispatch = useAuthDispatch(); //get the dispatch method from the useDispatch custom hook

  const handleRegister = async (e) => {
    e.preventDefault();
    let payload = { email, password, username };
    try {
      let response = await registerUser(dispatch, payload); //RegisterUser action makes the request and handles all the neccessary state changes
      if (!response.user) return;
      history.push("/timeline"); //navigate to dashboard on success
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="authenticateContainer">
        <div className="authenticateContainerHeader">
          <h1>Inscription</h1>
          <p>Vous avez déjà un compte ? <Link to="/login">Connexion</Link></p>
        </div>
        <form>
          <div className={"registerForm"}>
            <div>
              <img src={`/assets/img/icon-username.svg`} />
              <div className={"registerFormItem"}>
                <input
                  type="text"
                  id="username"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  placeholder="Nom d'utilisateur"
                />
                <div className="border-field"></div>
              </div>
            </div>
            <div>
              <img src={`/assets/img/icon-person.svg`} />
              <div className={"registerFormItem"}>
                <input
                  type="text"
                  id="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Email"
                />
                <div className="border-field"></div>
              </div>
            </div>
            <div>
              <img src={`/assets/img/icon-lock.svg`} />
              <div className={"registerFormItem"}>
                <input
                  type="password"
                  id="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="Mot de passe"
                />
                <div className="border-field"></div>
              </div>
            </div>
            <div className="registerCheckbox">
              <input type="checkbox" />
              <label>Vous acceptez les conditions d’utilisations et la politique de confidentialité de TimeLine</label>
            </div>
          </div>
          <button onClick={handleRegister}>Inscription</button>
        </form>
      </div>
    </div>
  );
}

export default Register
