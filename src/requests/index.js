import events from './events';
import timelines from './timelines';

const requests = {
  events,
  timelines,
};

export default requests;
