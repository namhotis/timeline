const timelines = {
  get() {
    return fetch(`${process.env.REACT_APP_API_URL}timelines`);
  },
};

export default timelines;
